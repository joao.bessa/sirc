package br.com.sirc.util;

import static org.junit.Assert.*;

import org.junit.Test;

import br.com.sirc.persistencia.modelo.Etapa;

public class SircUtilTest {

	@Test
	public void entidadeValidaTest() {
		
		Etapa entidade = new Etapa();
		entidade.setId(1L);
		
		assertTrue(SircUtil.isValido(entidade));
		
	}
	
	@Test
	public void entidadeIdNuloTest(){
		assertFalse(SircUtil.isValido(new Etapa()));
	}
	
	@Test
	public void entidadeNulaTest(){
		Etapa entidade = null;
		
		assertFalse(SircUtil.isValido(entidade));
	}

}
