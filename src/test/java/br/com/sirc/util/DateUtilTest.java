package br.com.sirc.util;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

public class DateUtilTest {

	@Test
	public void getAnoAtualTest() {
		LocalDate dataAtual = LocalDate.now();
		
		assertNotNull(DateUtil.getAnoAtual());
		assertEquals(DateUtil.getAnoAtual(), dataAtual.getYear());
		
	}

}
