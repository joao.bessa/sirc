package br.com.sirc.negocio.exception;

import javax.ejb.ApplicationException;

import br.com.sirc.exceptions.SircException;

@ApplicationException(rollback = true)
public class NegocioException extends SircException {

	private static final long serialVersionUID = 1L;

	public NegocioException(String mensagem) {
		super(mensagem);
	}
	
	public NegocioException(Throwable excecao){
		super(excecao);
	}
	
	public NegocioException(String mensagem, Throwable excecao){
		super(mensagem, excecao);
	}


}
