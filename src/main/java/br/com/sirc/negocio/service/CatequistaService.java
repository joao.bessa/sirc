package br.com.sirc.negocio.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.sirc.negocio.exception.NegocioException;
import br.com.sirc.negocio.service.comum.AbstractCrudService;
import br.com.sirc.persistencia.dao.CatequistaDAO;
import br.com.sirc.persistencia.exceptions.PersistenciaException;
import br.com.sirc.persistencia.modelo.Catequista;

@Stateless
public class CatequistaService extends AbstractCrudService<Catequista, CatequistaDAO> {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private CatequistaDAO dao;

	@Override
	protected CatequistaDAO getDao() {
		return dao;
	}
	
	@Override
	public void validarDuplicidade(Catequista entidade) throws NegocioException {
		if(entidade != null) {
			try {
				getDao().validarRegistroJaExistente(entidade);
			} catch (PersistenciaException e) {
				apresentarMensagemErro(e.getMessage());
				throw new NegocioException(e.getMessage(), e);
			}
		}
	}

}
