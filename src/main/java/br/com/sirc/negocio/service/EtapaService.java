package br.com.sirc.negocio.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.sirc.negocio.service.comum.AbstractCrudService;
import br.com.sirc.persistencia.dao.EtapaDAO;
import br.com.sirc.persistencia.modelo.Etapa;

@Stateless
public class EtapaService extends AbstractCrudService<Etapa, EtapaDAO> {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EtapaDAO dao;

	@Override
	protected EtapaDAO getDao() {
		return dao;
	}

}
