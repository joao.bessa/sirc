package br.com.sirc.negocio.service.comum;

import java.io.Serializable;

import javax.ejb.Stateless;

import br.com.sirc.enums.TipoMensagemEnum;
import br.com.sirc.util.FacesUtil;

/**
 * 
 * @author João Pedro
 * @since 23/11/2016
 * @category Comum
 * 
 * Classe pai das classes genéricas da camada de negócio
 *
 */
@Stateless
public abstract class Service implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Método que adiciona uma mensagem de sucesso na tela
	 * @param tipoMensagem
	 * @param detalhe
	 */
	protected void apresentarMensagemSucesso(String detalhe){
		FacesUtil.configurarMensagem(TipoMensagemEnum.SUCESSO, detalhe);
	}

	/**
	 * Método que adiciona uma mensagem de erro na tela
	 * @param tipoMensagem
	 * @param detalhe
	 */
	protected void apresentarMensagemErro(String detalhe){
		FacesUtil.configurarMensagem(TipoMensagemEnum.ERRO, detalhe);
	}

}
