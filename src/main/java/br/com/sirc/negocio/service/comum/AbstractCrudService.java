package br.com.sirc.negocio.service.comum;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;

import br.com.sirc.negocio.exception.NegocioException;
import br.com.sirc.persistencia.dao.comum.AbstractCrudDAO;
import br.com.sirc.persistencia.exceptions.PersistenciaException;
import br.com.sirc.persistencia.modelo.comum.AbstractComumEntidade;

/**
 * 
 * @author João Pedro
 * @since 23/11/2016
 * @category Comum
 * @param <Entidade>
 * @param <Dao>
 * 
 * Classe que contém método de CRUD para fluxos com entidades
 */
@Stateless
public abstract class AbstractCrudService<Entidade extends AbstractComumEntidade<?>, Dao extends AbstractCrudDAO<Entidade>> extends Service {

	private static final long serialVersionUID = 1L;
	
	private static final String MSG_SUCESSO = "Operação realizada com sucesso";
	
	/**
	 * Método que deve ser implementado por todas as filhas.
	 * Utilizado em toda a classe para retornar a instância da DAO
	 * @return Dao
	 */
	protected abstract Dao getDao();
	
	/**
	 * Método que valida se o registro já foi incluído na base.
	 * Para que seja utilizado, deve ser implementado pelas classes filhas.
	 * @param entidade
	 * @return boolean
	 * @throws NegocioException 
	 */
	public void validarDuplicidade(Entidade entidade) throws NegocioException{
	}
	
	/**
	 * Método responsável por salvar o objeto na base de dados
	 * @param entidade
	 * @return Entidade
	 */
	public Entidade salvar(Entidade entidade){
		try {
			entidade = getDao().salvar(entidade);
			apresentarMensagemSucesso(MSG_SUCESSO);
		} catch (PersistenciaException e) {
			apresentarMensagemErro(e.getMessage());
		}
		return entidade;
	}
	
	/**
	 * Método responsável por excluir os dados da base.
	 * @param id
	 */
	public void excluir(Serializable id){
		try {
			getDao().excluir(id);
			apresentarMensagemSucesso(MSG_SUCESSO);
		} catch (PersistenciaException e) {
			apresentarMensagemErro(e.getMessage());
		}
	}
	
	/**
	 * Método responsável por recuperar um registro pelo seu ID
	 * @param id
	 * @return Entidade
	 */
	public Entidade obter(Serializable id){
		try{
			return getDao().recuperarPorId(id);
		}catch (PersistenciaException e){
			apresentarMensagemErro(e.getMessage());
			return null;
		}
	}
	
	/**
	 * Método responsável por buscar todos os itens de uma determinada tabela
	 * @return List
	 */
	public List<Entidade> listar(){
		try{
			return getDao().listar();
		}catch (PersistenciaException e){
			apresentarMensagemErro(e.getMessage());
			return null;
		}
	}

}
