package br.com.sirc.enums;

/**
 * 
 * @author João Pedro
 * @since 25/02/2017
 * @category Comum
 * 
 * Enum que contém parametros gerais do sistema
 *
 */
public enum ParametrosEnum {
	
	TOTAL_ANOS("10");
	
	private String valorParametro;
	
	/**
	 * Construtor privado
	 * @param valorParametro
	 */
	private ParametrosEnum(String valorParametro) {
		this.valorParametro = valorParametro;
	}

	/**
	 * Recupera o valor do parâmetro em questão
	 * @return valor do parâmetro
	 */
	public String getValorParametro() {
		return valorParametro;
	}
	
}
