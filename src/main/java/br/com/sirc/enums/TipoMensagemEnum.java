package br.com.sirc.enums;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;

/**
 * 
 * @author João Pedro
 * @since 23/11/2016
 * @category Comum
 * 
 * Enum que detém os tipos de mensagens utilizados pelo sistema
 *
 */
public enum TipoMensagemEnum {
	
	SUCESSO(FacesMessage.SEVERITY_INFO, "Sucesso"),
	ERRO(FacesMessage.SEVERITY_ERROR, "Erro"),
	AVISO(FacesMessage.SEVERITY_WARN, "Aviso"),
	FATAL(FacesMessage.SEVERITY_FATAL, "Fatal");
	
	Severity severity;
	String titulo;
	
	private TipoMensagemEnum(Severity severity, String titulo){
		this.severity = severity;
		this.titulo = titulo;
	}
	
	public Severity getSeverity(){
		return severity;
	}
	
	public String getTitulo(){
		return titulo;
	}

}
