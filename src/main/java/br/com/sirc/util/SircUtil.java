package br.com.sirc.util;

import br.com.sirc.persistencia.modelo.comum.AbstractComumEntidade;

/**
 * 
 * @author João Pedro
 * @since 30/11/2016
 * @category Util
 * 
 * Classe que contém métodos utilitários que serão utilizados para o sistema.
 *
 */
public final class SircUtil {
	
	/**
	 * Construtor privado
	 */
	private SircUtil(){
	}
	
	/**
	 * Método que valida se a entidade e seu id estão nula
	 * @param entidade
	 * @return Verdadeiro, caso o objeto seja válido e falso caso contrário
	 */
	public static <Entidade extends AbstractComumEntidade<?>> boolean isValido(Entidade entidade){
		return entidade != null && entidade.getIdentificador() != null;
	}

}
