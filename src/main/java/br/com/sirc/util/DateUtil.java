package br.com.sirc.util;

import java.time.LocalDate;

/**
 * 
 * @author João Pedro
 * @since 25/02/2017
 * @category Util
 * 
 * Classe que contém métodos utilitários para se trabalhar com datas
 *
 */
public final class DateUtil {

	/**
	 * Construtor privado
	 */
	private DateUtil(){
	}
	
	/**
	 * Recupera o ano atual
	 * @return ano em exercício
	 */
	public static int getAnoAtual() {
		return LocalDate.now().getYear();
	}

}
