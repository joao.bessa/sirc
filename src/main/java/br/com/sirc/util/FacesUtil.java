package br.com.sirc.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import br.com.sirc.enums.TipoMensagemEnum;

/**
 * 
 * @author João Pedro
 * @since 23/11/2016
 * @category Util
 * 
 * Classe que contém métodos utilitários para auxiliar na integração com o JSF
 *
 */
public final class FacesUtil {
	
	/**
	 * Construtor privado
	 */
	private FacesUtil(){
	}
	
	/**
	 * Método que configura as mensagens na tela
	 * @param tipoMensagem
	 * @param detalhe
	 */
	public static void configurarMensagem(TipoMensagemEnum tipoMensagem, String detalhe) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(tipoMensagem.getSeverity(), tipoMensagem.getTitulo(), detalhe)); 
	}
 
}
