package br.com.sirc.exceptions;

/**
 * 
 * @author João Pedro
 * @since 28/11/2016
 * @category Exception
 * 
 * Super classe de exceções do sistema.
 * Possui métodos que serão utilizados pelas outras exceções do sistema.
 *
 */
//TODO Implementar aqui a parte que buscará no arquivo as mensagens
public class SircException extends Exception {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Construtor somente de String
	 * @param mensagem
	 */
	public SircException(String mensagem){
		super(mensagem);
	}
	
	/**
	 * Construtor somente de exceção
	 * @param excecao
	 */
	public SircException(Throwable excecao){
		super(excecao);
	}
	
	/**
	 * Construtor de mensagem(String) e exceção
	 * @param mensagem
	 * @param excecao
	 */
	public SircException(String mensagem, Throwable excecao){
		super(mensagem, excecao);
	}

}
