package br.com.sirc.view.converters;

import java.util.Arrays;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;

import br.com.sirc.persistencia.modelo.enums.EstadoCivilEnum;

/**
 * @author João Pedro
 * @since 11/12/2016
 * @category Enum
 * 
 * Classe que faz a convesão de estados civil de valores que vem da base de dados
 *
 */
@FacesConverter(value = "estadoCivilConverter")
public class EstadoCivilConverter implements Converter {

	/*
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
	 */
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		return recuperarEstadoCivil((Integer) value);
	}
	
	/**
	 * Recupera a descrição correta para o valor passado
	 * @param codigo
	 * @return String
	 */
	public String recuperarEstadoCivil(Integer codigo){
		for(EstadoCivilEnum estadoCivil : Arrays.asList(EstadoCivilEnum.values())){
			if (codigo.equals(estadoCivil.getCodEstadoCivil()))
				return estadoCivil.getDescEstadoCivil();
		}
		
		return StringUtils.EMPTY;
	}

}
