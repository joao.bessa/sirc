package br.com.sirc.view.converters;

import java.util.Arrays;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;

import br.com.sirc.persistencia.modelo.enums.SexoEnum;

/**
 * @author João Pedro
 * @since 10/12/2016
 * @category Enum
 * 
 * Classe que faz a conversão de sexo de valores que vem da base de dados
 *
 */
@FacesConverter (value = "sexoConverter")
public class SexoConverter implements Converter {

	/*
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
	 */
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		return recuperarSexo((Integer) value);
	}
	
	/**
	 * Recuepra a descrição correta para o valor passado
	 * @param codigo
	 * @return String
	 */
	private String recuperarSexo(Integer codigo){
		
		for(SexoEnum sexo : Arrays.asList(SexoEnum.values())) {
			if (codigo.equals(sexo.getCodSexo()))
				return sexo.getDescSexo();
		}
		
		return StringUtils.EMPTY;
		
	}

}
