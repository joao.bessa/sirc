package br.com.sirc.view.converters;

import java.util.Arrays;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;

import br.com.sirc.persistencia.modelo.enums.StatusEnum;

/**
 * 
 * @author João Pedro
 * @since 29/11/2016
 * @category Converter
 * 
 * Classe que faz a conversão de valores de status que veêm da base de dados
 *
 */
@FacesConverter(value = "statusConverter")
public class StatusConverter implements Converter {

	/*
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.String)
	 */
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		return recuperarStatus((Integer) value);
	}
	
	/**
	 * Compara o valor e recupera a descrição correta do valor passado
	 * @param codigo
	 * @return String
	 */
	private String recuperarStatus(Integer codigo){
		
		for(StatusEnum status : Arrays.asList(StatusEnum.values())){
			if (codigo.equals(status.getCodStatus()))
				return status.getDescStatus();
		}
		
		return StringUtils.EMPTY;
	}

}
