package br.com.sirc.view.controllers.catequista;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import br.com.sirc.negocio.service.CatequistaService;
import br.com.sirc.persistencia.modelo.Catequista;
import br.com.sirc.view.controllers.comum.AbstractCrudBean;

@ManagedBean
@ViewScoped
public class PesquisaCatequisaBean extends AbstractCrudBean<Catequista, CatequistaService> {

	private static final long serialVersionUID = 1L;

	@Inject
	private CatequistaService service;
	
	@Override
	protected CatequistaService getServico() {
		return service;
	}

	@PostConstruct
	@Override
	public void inicializar() {
		listar();
	}

}
