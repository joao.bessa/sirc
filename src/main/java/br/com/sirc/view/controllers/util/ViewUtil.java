package br.com.sirc.view.controllers.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.sirc.enums.ParametrosEnum;
import br.com.sirc.persistencia.modelo.enums.BooleanoEnum;
import br.com.sirc.persistencia.modelo.enums.EstadoCivilEnum;
import br.com.sirc.persistencia.modelo.enums.SexoEnum;
import br.com.sirc.persistencia.modelo.enums.StatusEnum;
import br.com.sirc.util.DateUtil;

/**
 * 
 * @author João Pedro
 * @since 06/02/2017
 * @category Util
 * 
 * Classe utilitária que contém métodos comuns entre os controllers
 *
 */
public final class ViewUtil {
	
	/**
	 * Construtor privado
	 */
	private ViewUtil(){
	}
	
	/**
	 * Método responsável por configurar uma lista de status de acordo com o enum
	 * @return lista de valores do enum
	 */
	public static List<StatusEnum> configurarCampoStaus(){
		return Arrays.asList(StatusEnum.values());
	}
	
	/**
	 * Método responsável por configurar uma lista de sexo de acordo com o enum
	 * @return lista de valores do enum
	 */
	public static List<SexoEnum> configurarCampoSexo(){
		return Arrays.asList(SexoEnum.values());
	}
	
	/**
	 * Método responsável por configurar uma lista de estado civil de acordo com o enum
	 * @return lista de valores do enum
	 */
	public static List<EstadoCivilEnum> configurarCampoEstadoCivil(){
		return Arrays.asList(EstadoCivilEnum.values());
	}
	
	/**
	 * Método responsável por configurar uma lista de booleano(sim/não) de acordo com o enum
	 * @return lista de valores do enum
	 */
	public static List<BooleanoEnum> configurarCampoBooleano(){
		return Arrays.asList(BooleanoEnum.values());
	}
	
	/**
	 * Método responsável por montar a combo de anos
	 * @return
	 */
	public static List<Integer> configurarComboAno(){
		Integer totalAnos = Integer.valueOf(ParametrosEnum.TOTAL_ANOS.getValorParametro());
		Integer anoAtual = DateUtil.getAnoAtual();
		List<Integer> anos = new ArrayList<>();
		
		for(int i = 0; i < totalAnos; i++){
			anos.add(anoAtual - i);
		}
		
		return anos;
	}
	
}
