package br.com.sirc.view.controllers.comum;

import java.util.ArrayList;
import java.util.List;

import br.com.sirc.negocio.exception.NegocioException;
import br.com.sirc.negocio.service.comum.AbstractCrudService;
import br.com.sirc.persistencia.modelo.comum.AbstractComumEntidade;

/**
 * 
 * @author João Pedro
 * @since 30/11/2016
 * @category Comum
 *
 * @param <Servico>
 * @param <Entidade>
 * 
 * Super classe que deve ser extendida por fluxos que trabalham com entidades
 * 
 */
public abstract class AbstractCrudBean<Entidade extends AbstractComumEntidade<?>, Servico extends AbstractCrudService<Entidade,?>> extends Bean {

	private static final long serialVersionUID = 1L;
	
	private Entidade entidade;
	private Entidade entidadeSelecionada;
	
	private List<Entidade> entidades = new ArrayList<>();

	/**
	 * Método que deve ser implementado por todas as filhas.
	 * Retorna a instãncia do serviço apontado acima.
	 * @return Servico
	 */
	protected abstract Servico getServico();
	
	/**
	 * Método responsável por carregar as informações iniciais da tela.
	 * Deve ser implementado em todas as filhas.
	 * Deve ser anotado com @PostConstruct
	 */
	public abstract void inicializar();

	/**
	 * Método que deve ser sobrescrito nas classes que tratarão de cadastros
	 */
	public void limpar(){
	}
	
	/**
	 * Método responsável por enviar a entidade para ser salva ou alterada na base
	 * @throws NegocioException 
	 */
	public void salvar() {
		try {
			getServico().validarDuplicidade(getEntidade() != null ? getEntidade() : null);
			getServico().salvar(getEntidade() != null ? getEntidade() : getEntidadeSelecionada());
			limpar();
		} catch (NegocioException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Método responsável por apagar um registro selecionado da base
	 */
	public void excluir(){
		getServico().excluir(getEntidadeSelecionada().getIdentificador());
		this.entidades.remove(getEntidadeSelecionada());
	}
	
	/**
	 * Método responsável por listar os objetos da base
	 */
	public void listar(){
		setEntidades(getServico().listar());
	}
	

	public Entidade getEntidade() {
		return entidade;
	}

	public void setEntidade(Entidade entidade) {
		this.entidade = entidade;
	}

	public Entidade getEntidadeSelecionada() {
		return entidadeSelecionada;
	}

	public void setEntidadeSelecionada(Entidade entidadeSelecionada) {
		this.entidadeSelecionada = entidadeSelecionada;
	}

	public List<Entidade> getEntidades() {
		return entidades;
	}

	public void setEntidades(List<Entidade> entidades) {
		this.entidades = entidades;
	}
	
}
