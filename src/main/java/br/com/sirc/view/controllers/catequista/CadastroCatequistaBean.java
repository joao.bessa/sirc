package br.com.sirc.view.controllers.catequista;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import br.com.sirc.negocio.service.CatequistaService;
import br.com.sirc.persistencia.modelo.Catequista;
import br.com.sirc.persistencia.modelo.enums.BooleanoEnum;
import br.com.sirc.persistencia.modelo.enums.EstadoCivilEnum;
import br.com.sirc.persistencia.modelo.enums.SexoEnum;
import br.com.sirc.persistencia.modelo.enums.StatusEnum;
import br.com.sirc.view.controllers.comum.AbstractCrudBean;
import br.com.sirc.view.controllers.util.ViewUtil;

@ManagedBean
@ViewScoped
public class CadastroCatequistaBean extends AbstractCrudBean<Catequista, CatequistaService> {

	private static final long serialVersionUID = 1L;

	@Inject
	private CatequistaService service;
	
	@Override
	protected CatequistaService getServico() {
		return service;
	}

	@PostConstruct
	@Override
	public void inicializar() {
		limpar();
	}
	
	@Override
	public void limpar() {
		setEntidade(new Catequista());
	}
	
	public boolean bloquearCampoConjugue() {
		return !(getEntidade().getEstadoCivil() != null && 
				getEntidade().getEstadoCivil().equals(EstadoCivilEnum.CASADO.getCodEstadoCivil()));
	}
	
	public List<SexoEnum> getRadioSexo() {
		return ViewUtil.configurarCampoSexo();
	}
	
	public List<EstadoCivilEnum> getRadioEstadoCivil() {
		return ViewUtil.configurarCampoEstadoCivil();
	}
	
	public List<StatusEnum> getRadioStatus(){
		return ViewUtil.configurarCampoStaus();
	}
	
	public List<BooleanoEnum> getRadioBooleano(){
		return ViewUtil.configurarCampoBooleano();
	}
	
	public List<Integer> getComboAno(){
		return ViewUtil.configurarComboAno();
	}
	
}
