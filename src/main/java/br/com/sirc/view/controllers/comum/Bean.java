package br.com.sirc.view.controllers.comum;

import java.io.Serializable;

/**
 * 
 * @author João Pedro
 * @since 30/11/2016
 * @category Comum
 * 
 * Super classe das classes genéricas que serão herdadas pelo restante das Bean's
 *
 */
public abstract class Bean implements Serializable{

	private static final long serialVersionUID = 1L;
}
