package br.com.sirc.view.controllers.etapa;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import br.com.sirc.negocio.service.EtapaService;
import br.com.sirc.persistencia.modelo.Etapa;
import br.com.sirc.view.controllers.comum.AbstractCrudBean;

@ManagedBean
@ViewScoped
public class CadastroEtapaBean extends AbstractCrudBean<Etapa, EtapaService> {

	private static final long serialVersionUID = 1L;

	@Inject
	private EtapaService service;
	
	@Override
	protected EtapaService getServico() {
		return service;
	}

	@PostConstruct
	@Override
	public void inicializar() {
		limpar();
	}
	
	@Override
	public void limpar() {
		setEntidade(new Etapa());
	}

}
