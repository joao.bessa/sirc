package br.com.sirc.persistencia.exceptions;

import br.com.sirc.exceptions.SircException;

/**
 * 
 * @author João Pedro
 * @since 23/11/2016
 * @category Exception
 * 
 * Exceção que deverá ser lançada de toda a camada de persistência
 *
 */
//TODO Joao Bessa alterar estrutura para que as exceções utilizem uma em comum que buscará a chave das
// mensagens em um arquivo .properties
public class PersistenciaException extends SircException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Construtor somente de String
	 * @param mensagem
	 */
	public PersistenciaException(String mensagem){
		super(mensagem);
	}
	
	
	/**
	 * Construtor somente de exceção
	 * @param excecao
	 */
	public PersistenciaException(Throwable excecao){
		super(excecao);
	}
	
	
	/**
	 * Construtor de mensagem(String) e exceção
	 * @param mensagem
	 * @param excecao
	 */
	public PersistenciaException(String mensagem, Throwable excecao){
		super(mensagem, excecao);
	}

}
