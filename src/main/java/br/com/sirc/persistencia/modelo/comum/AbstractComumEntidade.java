package br.com.sirc.persistencia.modelo.comum;

import java.io.Serializable;

/**
 * 
 * @author João Pedro
 * @since 23/11/2016
 * @category Comum
 * @param Id
 * 
 * Classe a ser extendida por todas as entidades do sistema
 */
public abstract class AbstractComumEntidade<Id extends Serializable> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Método que deve ser implementado por toda classe filha para indicar o Id da classe modelo
	 * @return Id
	 */
	public abstract Id getIdentificador();
	
	/**
	 * Método que verifica se o objeto e seu identificador estão nulos
	 * @return boolean
	 */
	public boolean isValido(){
		return getIdentificador() != null;
	}

}
