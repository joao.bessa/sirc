package br.com.sirc.persistencia.modelo.enums;

/**
 * 
 * @author João Pedro
 * @since 06/02/2017
 * @category Enum
 * 
 * Enum a ser utilizado para campos booleanos
 *
 */
public enum BooleanoEnum {
	
	SIM(1, "Sim"),
	NAO(2, "Não");
	
	private Integer codigo;
	private String descricao;
	
	private BooleanoEnum(Integer codigo, String descricao){
		this.codigo = codigo;
		this.descricao = descricao;
	}

	/**
	 * Recupera o codigo da escolha
	 * @return código da escolha
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * Recupera a descrição da escolha
	 * @return descricao da escolha
	 */
	public String getDescricao() {
		return descricao;
	}
	
}
