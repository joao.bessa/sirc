package br.com.sirc.persistencia.modelo.enums;

/**
 * 
 * @author João Pedro
 * @since 29/11/2016
 * @category Enum
 * 
 * Enum a ser utilizado como uma tabela domínio
 *
 */
public enum StatusEnum {
	
	ATIVO(1, "Ativo"),
	INATIVO(2, "Inativo"),
	PENDENTE(3, "Pendente"),
	CONCLUÍDO(4, "Concluído"),
	ARQUIVADO(5, "Arquivado");

	private Integer codStatus;
	private String descStatus;
	
	private StatusEnum(Integer codStatus, String descStatus){
		this.codStatus = codStatus;
		this.descStatus = descStatus;
	}

	/**
	 * Recupera o código do status
	 * @return código da opção selecionada
	 */
	public Integer getCodStatus() {
		return codStatus;
	}

	/**
	 * Recupera a descrição do status
	 * @return descrição da opção selecionada
	 */
	public String getDescStatus() {
		return descStatus;
	}
	
}
