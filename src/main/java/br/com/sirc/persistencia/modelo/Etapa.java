package br.com.sirc.persistencia.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.sirc.persistencia.modelo.comum.AbstractComumEntidade;

@Entity
@Table(name = "ETAPA", schema = "SIRC")
@SequenceGenerator(name = "ETAPA_ID", schema = "SIRC", allocationSize = 1, initialValue = 1, sequenceName = "SIRC.ETAPA_ID")
public class Etapa extends AbstractComumEntidade<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ETAPA_ID")
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "DESCRICAO", length = 100)
	@NotEmpty
	private String descricao;
	
	@Column(name = "IDADEMINIMA", length = 2)
	@NotNull
	private Integer idadeMinima;
	
	@Column(name = "IDADEMAXIMA", length = 2)
	private Integer idadeMaxima;

	public Long getId() {
		return id;
	}
	public void setId(Long codigo) {
		this.id = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Integer getIdadeMinima() {
		return idadeMinima;
	}
	public void setIdadeMinima(Integer idadeMinima) {
		this.idadeMinima = idadeMinima;
	}
	public Integer getIdadeMaxima() {
		return idadeMaxima;
	}
	public void setIdadeMaxima(Integer idadeMaxima) {
		this.idadeMaxima = idadeMaxima;
	}
	
	@Override
	public Long getIdentificador() {
		return getId();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Etapa other = (Etapa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
