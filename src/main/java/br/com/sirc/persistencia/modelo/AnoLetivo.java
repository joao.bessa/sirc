package br.com.sirc.persistencia.modelo;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.com.sirc.persistencia.converters.LocalDateConverter;
import br.com.sirc.persistencia.modelo.comum.AbstractComumEntidade;

@Entity
@Table(schema = "SIRC", name = "ANOLETIVO")
public class AnoLetivo extends AbstractComumEntidade<Integer> {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ANO", length = 4)
	@NotNull
	private Integer ano;
	
	@Convert(converter = LocalDateConverter.class)
	@Column(name = "DATAINICIO")
	@NotNull
	private LocalDate dataInicio;
	
	@Convert(converter = LocalDateConverter.class)
	@Column(name = "DATAFIM")
	@NotNull
	private LocalDate dataFim;
	
	@Column(name = "BOLFECHADO")
	@NotNull
	private Boolean bolFechado;

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public LocalDate getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}

	public LocalDate getDataFim() {
		return dataFim;
	}

	public void setDataFim(LocalDate dataFim) {
		this.dataFim = dataFim;
	}

	public Boolean getBolFechado() {
		return bolFechado;
	}

	public void setBolFechado(Boolean bolFechado) {
		this.bolFechado = bolFechado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ano == null) ? 0 : ano.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnoLetivo other = (AnoLetivo) obj;
		if (ano == null) {
			if (other.ano != null)
				return false;
		} else if (!ano.equals(other.ano))
			return false;
		return true;
	}

	@Override
	public Integer getIdentificador() {
		return getAno();
	}
	

}
