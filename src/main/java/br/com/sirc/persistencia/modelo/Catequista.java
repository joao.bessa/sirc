package br.com.sirc.persistencia.modelo;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;

import br.com.sirc.persistencia.converters.LocalDateConverter;
import br.com.sirc.persistencia.modelo.comum.AbstractComumEntidade;

@Entity
@Table (name = "CATEQUISTA", schema = "SIRC")
public class Catequista extends AbstractComumEntidade<String> {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column (name = "CPF", length = 14)
	@NotEmpty
	@CPF
	private String cpf;
	
	@Column (name = "NOME", length = 500)
	@NotEmpty
	private String nome;
	
	@Column (name = "DESCENDERECO", length = 500)
	private String endereco;
	
	@Column (name = "CIDADE", length = 100)
	private String cidade;
	
	@Column (name = "TELEFONE", length = 10)
	private String telefone;
	
	@Column (name = "CELULAR", length = 10)
	private String celular;
	
	@Convert(converter = LocalDateConverter.class)
	@Column (name = "DATANASCIMENTO")
	@NotNull
	private LocalDate dataNascimento;
	
	@Column (name = "EMAIL", length = 200)
	private String email;
	
	@Column (name = "SEXO")
	@NotNull
	private Integer sexo;
	
	@Column (name = "ESTADOCIVIL")
	@NotNull
	private Integer estadoCivil;
	
	@Column (name = "STATUS")
	private Integer status;
	
	@Column (name = "BOLEAC")
	private Integer bolEac;
	
	@Column (name = "DESCOBSERVACAO", length = 500)
	private String descObservacao;
	
	@Column (name = "ANOEAC")
	private Integer ano_eac;
	
	@Column (name = "DESCCONJUGUE", length = 200)
	private String descConjugue;
	
	@Column (name = "BOLCOORDENADOR")
	@NotNull
	private Integer bolCoordenador;
	
	@ManyToMany
	@JoinTable(name = "CATEQUISTATURMA", schema = "SIRC",
				joinColumns = @JoinColumn(name = "CPFCATEQUISTA", referencedColumnName = "CPF"),
				inverseJoinColumns = @JoinColumn(name = "IDTURMA", referencedColumnName = "ID"))
	private List<Turma> turmas;
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getSexo() {
		return sexo;
	}
	public void setSexo(Integer sexo) {
		this.sexo = sexo;
	}
	public Integer getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(Integer estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getBolEac() {
		return bolEac;
	}
	public void setBolEac(Integer bolEac) {
		this.bolEac = bolEac;
	}
	public String getDescObservacao() {
		return descObservacao;
	}
	public void setDescObservacao(String descObservacao) {
		this.descObservacao = descObservacao;
	}
	public Integer getAno_eac() {
		return ano_eac;
	}
	public void setAno_eac(Integer ano_eac) {
		this.ano_eac = ano_eac;
	}
	public String getDescConjugue() {
		return descConjugue;
	}
	public void setDescConjugue(String descConjugue) {
		this.descConjugue = descConjugue;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public Integer getBolCoordenador() {
		return bolCoordenador;
	}
	public void setBolCoordenador(Integer bolCoordenador) {
		this.bolCoordenador = bolCoordenador;
	}
	
	public List<Turma> getTurmas() {
		return turmas;
	}
	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}
	
	@Override
	public String getIdentificador() {
		return getCpf();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Catequista other = (Catequista) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		return true;
	}
	
}
