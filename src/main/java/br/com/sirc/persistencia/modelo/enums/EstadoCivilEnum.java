package br.com.sirc.persistencia.modelo.enums;

/**
 * 
 * @author João Pedro
 * @since 10/12/2016
 * @category Enum
 * 
 * Enum a ser utilizado como tabela de domínio
 *
 */
public enum EstadoCivilEnum {
	
	SOLTEIRO(1, "Solteiro(a)"),
	CASADO(2, "Casado(a)"),
	DIVORCIADO(3, "Divorciado(a)"),
	VIUVO(4, "Viúvo(a)");
	
	private Integer codEstadoCivil;
	private String descEstadoCivil;
	
	/**
	 * Construtor privado
	 * @param codEstadoCivil
	 * @param descEstadoCivil
	 */
	private EstadoCivilEnum(Integer codEstadoCivil, String descEstadoCivil){
		this.codEstadoCivil = codEstadoCivil;
		this.descEstadoCivil = descEstadoCivil;
	}

	/**
	 * Recupera o código do estado civil
	 * @return código do registro selecionado
	 */
	public Integer getCodEstadoCivil() {
		return codEstadoCivil;
	}
	
	
	/**
	 * Recupera a descrição do estado civil
	 * @return descrição do registro selecionado
	 */
	public String getDescEstadoCivil() {
		return descEstadoCivil;
	}
	
}
