package br.com.sirc.persistencia.modelo.enums;

/**
 * 
 * @author João Pedro
 * @since 10/12/2016
 * @category Enum
 * 
 * Enum a ser utilizado como tabela de domínio
 *
 */
public enum SexoEnum {
	
	MASCULINO(1, "Masculino"),
	FEMININO(2, "Feminino");
	
	private Integer codSexo;
	private String descSexo;

	/**
	 * Construtor privado com argumentos
	 * @param codSexo
	 * @param descSexo
	 */
	private SexoEnum(Integer codSexo, String descSexo){
		this.codSexo = codSexo;
		this.descSexo = descSexo;
	}
	
	/**
	 * Recupera o código do sexo
	 * @return código da opção selecionada
	 */
	public Integer getCodSexo() {
		return codSexo;
	}
	
	/**
	 * Recupera a descrição do sexo
	 * @return descrição da opção selecionada
	 */
	public String getDescSexo() {
		return descSexo;
	}
	
}
