package br.com.sirc.persistencia.modelo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.com.sirc.persistencia.modelo.comum.AbstractComumEntidade;

@Entity
@Table(name = "TURMA", schema = "SIRC")
@SequenceGenerator(name = "TURMA_ID", schema = "SIRC", allocationSize = 1, initialValue = 1, sequenceName = "SIRC.TURMA_ID")
public class Turma extends AbstractComumEntidade<Integer> {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TURMA_ID")
	@Column(name = "ID")
	@NotNull
	private Integer id;
	
	@Column(name = "SEQUENCIAL", length = 4)
	private String sequencial;
	
	@ManyToOne
	@JoinColumn(name = "IDETAPA", referencedColumnName = "ID")
	@NotNull
	private Etapa etapa;
	
	@ManyToOne
	@JoinColumn(name = "ANOLETIVO", referencedColumnName = "ANO")
	@NotNull
	private AnoLetivo anoLetivo;
	
	@Column(name = "MAXALUNOS", length = 2)
	private Integer maxAlunos;
	
	@Column(name = "QTDALUNOS", length = 2)
	private Integer qtdAlunos;
	
	@Column(name = "STATUS", length = 1)
	@NotNull
	private Integer status;
	
	@ManyToMany
	@JoinTable(name = "CATEQUISTATURMA", schema = "SIRC",
				joinColumns = @JoinColumn(name = "IDTURMA", referencedColumnName = "ID"),
				inverseJoinColumns = @JoinColumn(name = "CPFCATEQUISTA", referencedColumnName = "CPF"))
	private List<Catequista> catequistas;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSequencial() {
		return sequencial;
	}

	public void setSequencial(String sequencial) {
		this.sequencial = sequencial;
	}

	public Etapa getEtapa() {
		return etapa;
	}

	public void setEtapa(Etapa etapa) {
		this.etapa = etapa;
	}

	public AnoLetivo getAnoLetivo() {
		return anoLetivo;
	}

	public void setAnoLetivo(AnoLetivo anoLetivo) {
		this.anoLetivo = anoLetivo;
	}

	public Integer getMaxAlunos() {
		return maxAlunos;
	}

	public void setMaxAlunos(Integer maxAlunos) {
		this.maxAlunos = maxAlunos;
	}

	public Integer getQtdAlunos() {
		return qtdAlunos;
	}

	public void setQtdAlunos(Integer qtdAlunos) {
		this.qtdAlunos = qtdAlunos;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<Catequista> getCatequistas() {
		return catequistas;
	}

	public void setCatequistas(List<Catequista> catequistas) {
		this.catequistas = catequistas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Turma other = (Turma) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public Integer getIdentificador() {
		return getId();
	}
	
}
