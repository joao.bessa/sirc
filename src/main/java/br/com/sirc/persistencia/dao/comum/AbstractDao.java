package br.com.sirc.persistencia.dao.comum;

import javax.ejb.Stateless;

/**
 * 
 * @author João Pedro
 * @since 23/11/2016
 * @category Comum
 * 
 * Classe abstrata que deve ser extendida por fluxos que não trabalham com entidades
 *
 */
@Stateless
public abstract class AbstractDao extends DAO {

	private static final long serialVersionUID = 1L;

}
