package br.com.sirc.persistencia.dao;

import java.util.List;

import javax.ejb.Stateless;

import br.com.sirc.persistencia.dao.comum.AbstractCrudDAO;
import br.com.sirc.persistencia.exceptions.PersistenciaException;
import br.com.sirc.persistencia.modelo.Catequista;

@Stateless
public class CatequistaDAO extends AbstractCrudDAO<Catequista> {

	private static final long serialVersionUID = 1L;

	public CatequistaDAO() {
		super(Catequista.class);
	}

	@Override
	public List<Catequista> listar() throws PersistenciaException {
		return getEntityManager().createQuery("SELECT c FROM Catequista c", Catequista.class).getResultList();
	}

	public void validarRegistroJaExistente(Catequista entidade) throws PersistenciaException {
		String sql = "SELECT c FROM Catequista c WHERE 1=1 ";
		if(entidade.getCpf() != null) {
			sql += "AND c.cpf = '" + entidade.getCpf() + "'";
		}

		if(!(getEntityManager().createQuery(sql).getResultList().isEmpty())) {
			throw new PersistenciaException("Já existe um registro com as mesmas informações");
		}
	}
	
	

}
