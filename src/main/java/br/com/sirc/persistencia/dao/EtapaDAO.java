package br.com.sirc.persistencia.dao;

import java.util.List;

import javax.ejb.Stateless;

import br.com.sirc.persistencia.dao.comum.AbstractCrudDAO;
import br.com.sirc.persistencia.exceptions.PersistenciaException;
import br.com.sirc.persistencia.modelo.Etapa;

@Stateless
public class EtapaDAO extends AbstractCrudDAO<Etapa> {

	private static final long serialVersionUID = 1L;
	
	public EtapaDAO(){
		super(Etapa.class);
	}

	@Override
	public List<Etapa> listar() throws PersistenciaException {
		return getEntityManager().createQuery("SELECT e FROM Etapa e", Etapa.class).getResultList();
	}
	
}
