package br.com.sirc.persistencia.dao.comum;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 
 * @author João Pedro
 * @since 23/11/2016
 * @category Comum
 * 
 * Classe pai das genéricas que implementarão os fluxos na camada de persistência
 *
 */
@Stateless
public abstract class DAO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@PersistenceContext(unitName = "Sirc-PU")
	private EntityManager manager;
	
	/**
	 * Método que retorna o Manager retirado do contexto JTA
	 * @return
	 */
	protected EntityManager getEntityManager(){
		return this.manager;
	}

}
