package br.com.sirc.persistencia.dao.comum;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;

import br.com.sirc.persistencia.exceptions.PersistenciaException;
import br.com.sirc.persistencia.modelo.comum.AbstractComumEntidade;

/**
 * 
 * @author João Pedro
 * @since 23/11/2016
 * @category Comum
 * @param Entidade
 * 
 * Classe que deve ser extendida por todas as dao's que possuem fluxos vinculados à uma entidade(CRUD)
 */
@Stateless
public abstract class AbstractCrudDAO<Entidade extends AbstractComumEntidade<?>> extends DAO {

	private static final long serialVersionUID = 1L;
	
	private Class<Entidade> classType;
	
	//TODO passar essas mensagens para um arquivo '.properties'
	private static final String MSG_ERRO_BUSCAR = "Ocorreu um erro ao tentar recuperar o registo";
	private static final String MSG_ERRO_SALVAR = "Ocorreu um erro ao tentar salvar a informação";
	private static final String MSG_ERRO_EXCLUIR = "Ocorreu um erro ao tentar apagar a informação";
	
	/**
	 * Construtor que onde é informada o tipo de entidade que será trabalhada.
	 * Deve ser invocado em todas as filhas
	 * @param classType
	 */
	public AbstractCrudDAO(Class<Entidade> classType){
		this.classType = classType;
	}
	
	/**
	 * Método que deve ser implementado por todas as classes filhas.
	 * Lista todos os objetos de determinada tabela da base de dados.
	 * @return List
	 * @throws PersistenciaException 
	 */
	public abstract List<Entidade> listar() throws PersistenciaException;
	
	/**
	 * Método que recupera um registro pelo seu Id
	 * @param id
	 * @return Entidade
	 * @throws PersistenciaException 
	 */
	public Entidade recuperarPorId(Serializable id) throws PersistenciaException{
		try{
			return getEntityManager().find(classType, id);
		}catch(Exception e){
			throw new PersistenciaException(MSG_ERRO_BUSCAR, e);
		}
	}
	
	/**
	 * Método responsável por salvar as informações no banco de dados.
	 * @param entidade
	 * @return Entidade
	 * @throws PersistenciaException
	 */
	public Entidade salvar(Entidade entidade) throws PersistenciaException{
		try{
			getEntityManager().merge(entidade);
			getEntityManager().flush();
			return entidade;
		}catch(Exception e){
			throw new PersistenciaException(MSG_ERRO_SALVAR, e);
		}
	}
	
	/**
	 * Método responsável por apagar as informações da base de dados
	 * @param id
	 * @throws PersistenciaException
	 */
	public void excluir(Serializable id) throws PersistenciaException{
		try{
			getEntityManager().remove(getEntityManager().getReference(classType, id));
			getEntityManager().flush();
		}catch(Exception e){
			throw new PersistenciaException(MSG_ERRO_EXCLUIR, e);
		}
	}
	
}
