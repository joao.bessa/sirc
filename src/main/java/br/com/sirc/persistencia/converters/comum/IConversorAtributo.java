package br.com.sirc.persistencia.converters.comum;

import javax.persistence.AttributeConverter;

/**
 * 
 * @author João Pedro
 * @since 20/02/2017
 * @category Comum
 * 
 * Interface a ser implementada por todos os conversores de tipos para o JPA
 *
 * @param <AtributoEntidade>
 * @param <AtributoBD>
 */
public interface IConversorAtributo<AtributoEntidade, AtributoBD> extends AttributeConverter<AtributoEntidade, AtributoBD> {

}
