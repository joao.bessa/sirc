package br.com.sirc.persistencia.converters;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Converter;

import br.com.sirc.persistencia.converters.comum.IConversorAtributo;

/**
 * 
 * @author João Pedro
 * @since 20/02/2017
 * @category Converter
 * 
 * Conversor do tipo LocalDate(Java 8) para o Date(java.util)
 *
 */
@Converter(autoApply = true)
public class LocalDateConverter implements IConversorAtributo<LocalDate, Date> {

	/*
	 * (non-Javadoc)
	 * @see javax.persistence.AttributeConverter#convertToDatabaseColumn(java.lang.Object)
	 */
	@Override
	public Date convertToDatabaseColumn(LocalDate attribute) {
		Instant instant = attribute.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		return Date.from(instant);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.persistence.AttributeConverter#convertToEntityAttribute(java.lang.Object)
	 */
	@Override
	public LocalDate convertToEntityAttribute(Date dbData) {
		Instant instant = Instant.ofEpochMilli(dbData.getTime());
		return LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
	}

}
